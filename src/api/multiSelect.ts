import axios from 'axios';

export const getDatasets = () =>
  axios.get(
    'https://xn--80adgxdjdid1ar3isb.xn--p1ai/api/resources?categories_like=education&audiences_like=children&_page=1'
  );
