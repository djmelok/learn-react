import React, { useState, useEffect, useCallback, useMemo, useRef } from 'react';
import { ReactComponent as IconСross } from 'src/assets/icons/icon-cross.svg';
import { ReactComponent as IconRectangle } from 'src/assets/icons/icon-rectangle.svg';
import { OptionsType, OptionsUnionType } from 'src/types/multiSelect';
import './index.scss';
import classNames from 'classnames';
import CSS from 'csstype';

type MultiSelectProps = {
  options: OptionsUnionType[];
  value?: OptionsUnionType[];
  handleChange?: (array: OptionsUnionType[]) => void;
} & React.HTMLAttributes<HTMLDivElement>;

const MultiSelect: React.FC<MultiSelectProps> = ({ options, value, handleChange, className, ...props }) => {
  const [display, setDisplay] = useState<boolean>(false);
  const [list, setList] = useState<OptionsType[]>([]);
  const [selected, setSelected] = useState<OptionsType[]>([]);
  const style = useRef<CSS.Properties>({});

  useEffect(() => {
    options.forEach((item, idx) => {
      if (typeof item === 'string') {
        setList((array) => [
          ...array,
          {
            id: idx + 1,
            value: item,
            label: item,
          },
        ]);
      } else setList((array) => [...array, item]);
    });
  }, [options]);

  useEffect(() => {
    if (!value) return;
    const temp: OptionsType[] = [];
    value.forEach((item, idx) => {
      if (typeof item === 'string') {
        temp.push({
          id: list.find((el) => el.value === item)?.id || idx + 1,
          value: item as string,
          label: item as string,
        });
      } else temp.push(item);
    });

    setSelected(temp);
  }, [value]);

  const onClearItem = useCallback(
    (e: React.MouseEvent, item: OptionsType) => {
      e.stopPropagation();
      const filterSelected = selected.filter((el) => el.id !== item.id);
      if (handleChange) {
        handleChange(filterSelected.map((el) => {
          return options.includes(el.value) ? el.value : el;
        }));
      }

      setSelected(filterSelected);
    },
    [handleChange, selected]
  );

  const onClearAll = useCallback(
    (e: React.MouseEvent) => {
      e.stopPropagation();
      if (handleChange) handleChange([]);
      setSelected([]);
    },
    [handleChange]
  );

  const onCheckboxChange = useCallback(
    (e: React.ChangeEvent<HTMLInputElement>, item: OptionsType) => {
      e.stopPropagation();
      if (e.target.checked) {
        if (handleChange) {
          handleChange([...selected.map((el) => {
            return options.includes(el.value) ? el.value : el;
          }), options.includes(item.value) ? item.value : item]);
        }
        setSelected([...selected, item]);
      } else {
        const filterSelected = selected.filter((el) => el.id !== item.id);
        setSelected(filterSelected);
        if (handleChange) handleChange(filterSelected.map((el) => {
          return options.includes(el.value) ? el.value : el;
        }));
      }
    },
    [handleChange, selected]
  );

  const onToggleDisplay = (e: React.MouseEvent | React.KeyboardEvent) => {
    e.stopPropagation();
    setDisplay((item) => !item);
  };

  const templateCloseAll = (
    <button className="multiSelect__headInteractiveButton" onClick={onClearAll} type="button">
      <IconСross className="multiSelect__icon" />
    </button>
  );

  const templateSelected = selected.map((item) => {
    return (
      <button
        className="multiSelect__headContentValue"
        onClick={(e) => onClearItem(e, item)}
        key={item.id}
        type="button"
      >
        <span className="multiSelect__headContentValueText">{item.value}</span>
        <IconСross className="multiSelect__headContentValueIcon multiSelect__icon" />
      </button>
    );
  });

  const templateOptions = list.map((item) => {
    return (
      <li className="multiSelect__listItem" key={item.id}>
        <label className="multiSelect__listItemLabel">
          <input
            className="multiSelect__listItemLabelCheckbox"
            type="checkbox"
            checked={!!selected.find((el) => el.id === item.id)}
            onChange={(e) => onCheckboxChange(e, item)}
          />
          <span>{item.value}</span>
        </label>
      </li>
    );
  });

  useMemo(() => {
    const heightListItem = list.length * 35 + 22;
    const heightList = heightListItem < 200 ? heightListItem : 200;
    style.current = { height: `${display ? heightList : 0}px` };
  }, [list, display]);

  return (
    <div className={classNames('multiSelect', className)} {...props}>
      <div
        className="multiSelect__head"
        onClick={onToggleDisplay}
        onKeyPress={onToggleDisplay}
        role="button"
        tabIndex={0}
      >
        <div className="multiSelect__headContent">
          {selected.length ? (
            templateSelected
          ) : (
            <p className="multiSelect__headContentText">Выберите необходимые датасеты</p>
          )}
        </div>
        <div className="multiSelect__headInteractive">
          {selected.length ? templateCloseAll : null}
          <IconRectangle
            className={classNames('multiSelect__icon', 'multiSelect__headInteractiveIcon', {
              'multiSelect__headInteractiveIcon--rotate': !display,
            })}
          />
        </div>
      </div>
      <div style={style.current} className="multiSelect__wrapper">
        <ul className="multiSelect__list">{templateOptions}</ul>
      </div>
    </div>
  );
};

export default React.memo(MultiSelect);
