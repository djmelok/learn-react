export type OptionsType = {
  id: number;
  value: string;
  label: string;
};

export type OptionsUnionType = OptionsType | string;
