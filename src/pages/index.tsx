import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import HelloMST from 'src/components/HelloMST';
import MultiSelect from 'src/components/ui/MultiSelect/index';
import { setGreatDigitalAgency, setHelloMST } from 'src/store/actions/hello';
import { getDatasets } from 'src/api/multiSelect';
import { OptionsType, OptionsUnionType } from 'src/types/multiSelect';
import './index.scss';

type ApiDataType = {
  id: number;
  link: string;
  title: string;
};

const Page: React.FC = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(setHelloMST('Hello MST!'));
    const timer = setInterval(() => {
      dispatch(setGreatDigitalAgency('Great Digital Agency!'));
      setTimeout(() => {
        dispatch(setHelloMST('Hello MST!'));
      }, 1500);
    }, 3000);
    return () => clearInterval(timer);
  });

  const [dataFirst, setDataFirst] = useState<OptionsUnionType[]>([]);
  const [valueFirst, setValueFirst] = useState<OptionsUnionType[]>([]);

  const [dataSecond, setDataSecond] = useState<OptionsUnionType[]>([]);
  const [valueSecond, setValueSecond] = useState<OptionsUnionType[]>([]);

  useEffect(() => {
    const tempData: OptionsType[] = [];

    getDatasets()
      .then((r) => {
        r.data.forEach((item: ApiDataType) => {
          tempData.push({
            value: item.link,
            label: item.title,
            ...item
          });
        });
        setDataFirst(tempData.slice(0, 2));
        setDataSecond(tempData.map((el) => el.value));
        setValueFirst((array) => [...array, tempData[0]]);
      })
      .catch((e) => {
        console.error(e);
      });
  }, []);

  const handleChangeFirst = (array: OptionsUnionType[]) => {
    console.log('array1', array);
    setValueFirst(array);
  };

  const handleChangeSecond = (array: OptionsUnionType[]) => {
    console.log('array2', array);
    setValueSecond(array);
  };

  return (
    <div className="index">
      <HelloMST className="index__hello" />
      <MultiSelect className="index__select" value={valueFirst} handleChange={handleChangeFirst} options={dataFirst} />
      <MultiSelect
        className="index__select"
        value={valueSecond}
        handleChange={handleChangeSecond}
        options={dataSecond}
      />
    </div>
  );
};

export default React.memo(Page);
